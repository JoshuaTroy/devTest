<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Playlist;

class Track extends Model
{
    protected $fillable = [
        'name',
    ];

    public function playlists()
    {
        return $this->belongsToMany('App\Playlist', 'playlist_tracks', 'playlist_id', 'track_id');
    }
}
