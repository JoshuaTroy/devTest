<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Track;

class Playlist extends Model
{
    protected $fillable = [
        'name',
    ];

    public function tracks()
    {
        return $this->belongsToMany('App\Track', 'playlist_tracks', 'playlist_id', 'track_id');
    }
}
