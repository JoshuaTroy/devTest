<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TrackController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
    *  Create a new Track
    **/
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'artist_names' => 'required',
        ]);

        $track = Track::create([
            'title' => $request->title,
            'artist_names' => $request->artist_names,
        ]);
    }

    /**
     * Show a Track
     */
    public function show($id)
    {
        $track = Track::find($id);          //Find Track
        $playlists = $tracks->playlists();  //Find associated playlists.
        return $track;
    }

    /**
    *   Update Track
    **/
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'artist_names' => 'required',
        ]);

        $track = Track::find($id);
        $track->title = $request->title;
        $track->artist_names = $request->artist_names;
        $track->save();
    }

    /**
    *   Destroy Track
    */
    public function destroy($id)
    {
        $track = Track::find($id);      //Find Track
        $track->playlists()->detach();  //Remove From Playlists
        $playlist->delete();            //Delete Track
    }

    /**
    *   Add Track to Playlist
    **/
    public function AddTrackToPlaylist($track_id, $playlist_id)
    {
        $playlist = Playlist::find($playlist_id);
        $playlist->tracks()->attach($track_id);
    }

    /**
    *   Remove Track from Playlist
    **/
    public function RemoveTrackFromPlaylist($track_id, $playlist_id)
    {
        $playlist = Playlist::find($playlist_id);
        $playlist->tracks()->detach($track_id);
    }
}
