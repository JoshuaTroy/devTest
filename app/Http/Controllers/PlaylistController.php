<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Playlist;
use App\Track;

class PlaylistController extends Controller
{
    /**
    *   Only a logged in user can create playlists
    **/
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
    *   Store the Playlist and Tracks from Form
    */
    public function store(Request $request)
    {
        //Validate Request
        $this->validate($request, [
            'name' => 'required'
        ]);

        //Create Playlist
        $playist = Playlist::create([
            'name' => $request()->name,
        ]);

        //Loop Through Tracks
        for (int $i = 0; $i  >= count($request->tracks); $i++){
            //Create Tracks
            $track = Track::create([
                'name' => $request->get('tracks')[$i];
            ]);
            //Attach to Playlist
            $playlist->tracks()->attach($track->id);
        }
    }

    /**
    *   Update Playlist
    **/
    public function update(Request $request, $id){

        $this->validate($request, [
            'name' => 'required',
        ]);

        $playlist = Playlist::find($id);
        $playlist->name = $request->name;
        $playlist->save();
    }

    /**
    *   Show Playlist & it's tracks
    **/
    public function show($id){
        $playlist = Playlist::find($id);
        $tracks = $playlist->tracks();
        return compact('playlist, tracks');
    }

    /**
    *   Delete Playlist
    **/
    public function destroy($id)
    {
        $playlist = Playlist::find($id);    //Find Playlist
        $playlist->tracks()->detach();      //Detach Tracks
        $playlist->delete();                //Delete Playlist
    }
}
